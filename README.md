# examples

Example Woodpecker pipeline files (WIP).
Pull requests are accepted/welcome.

## Example pipeline files

| Link | Language | Build System | Comments |
| :--  | :--      | :--          | :--      |
| [C/make.yml](C/make.yml) | C | Make | Simple ci for building a Make based C project |
| [golang/build.yml](golang/build.yml) | golang | golang | Simple ci for building and test a Go project |
| [golang/build-docker.yml](golang/build-docker.yml) | golang | golang / kaniko | CI to build golang project and build various docker container and publish them on DockerHub |
| [Jekyll/jekyll.yml](Jekyll/jekyll.yml) | Markdown | Jekyll | CI step to build static website files and publish them to Codeberg Pages using Jekyll |
| [Hugo/hugo.yml](Hugo/hugo.yml) | Markdown | Hugo | CI step to build static website files and publish them to Codeberg Pages with Hugo |
| [Docker/kaniko.yml](Docker/kaniko.yml) | Dockerfile | [Kaniko][1] | Minimalistic CI pipeline with clear instructions to push a Docker image |
| [Docker/buildx.yml](Docker/buildx.yml) | Dockerfile | [buildx][2] | Build and publish Docker images for multiple architectures on codeberg |
|[Python/.woodpecker.yml](Python)| Python | Python venv| Standard CI pipeline to test Python packages code on multiple Python distributions. For details check   [serial-sphinx](https://codeberg.org/sail.black/serial-sphinx.git)|
# More:
https://codeberg.org/explore/repos?q=woodpecker-ci&topic=1

[1]: https://github.com/GoogleContainerTools/kaniko
[2]: https://codeberg.org/woodpecker-plugins/plugin-docker-buildx
